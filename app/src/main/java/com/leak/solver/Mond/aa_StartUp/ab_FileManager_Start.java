package com.leak.solver.Mond.aa_StartUp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.leak.solver.Mond.MainActivity;

import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_activity_home_screen;
import com.leak.solver.Mond.zz_Config;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class ab_FileManager_Start {



    private FirstTimeThread                                                  firstTimeThread = null;
    private SplashScreenThread                                            splashScreenThread = null;
//    ViewGroup                                                                           layout=null;
//    public static MyApplication                                                       _global =null;





    /** Global  */

    public ViewPager viewPager=null;
    public Map<String, List<String>> map =null;
    public List<String>                                                                 names =null;
    public Map<String, List<String>>                                              map_folder = null;
    public List<String>                                                         names_folder = null;
    public Boolean                                                              start_thread =true;
    public Boolean                                                             splash_thread =true;
    public Boolean                                                               home_thread =true;


    public   Intent                                                            intent=null;
    public  Boolean                                                      stop_intent =false;
    public  Boolean                                                   THREAD_STOPPER =false;



    final int numberOfPermits = 1;
    public Semaphore semaphore = new Semaphore(numberOfPermits, true);

    public  int  TOKEN_SCENE_CHANGE = 0;


    public boolean isFirstTime(Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean ranBefore = preferences.getBoolean("Permission", false);

        if(zz_Config.TEST)
            Log.d("_#FIRST_FLAG", "isFirstTime shared pref : aa_activity_StartUp " + ranBefore);

        return !ranBefore;
    }

    public void initialize_memory()
    {

        if(zz_Config.TEST){

            Log.d("_#_APP_MEM_MAKE", "onCreate: MyApplication make mem A ");

        }


        map                 = new HashMap<String, List<String>>();
        names               = new ArrayList<String>();
        map_folder          = new HashMap<String, List<String>>();
        names_folder        = new ArrayList<String>();

        if(zz_Config.TEST){

            Log.d("_#_APP_MEM_MAKE", "onCreate: MyApplication make mem B ");

        }



    }

    public void initialize()
    {

        firstTimeThread = new FirstTimeThread();
        splashScreenThread = new SplashScreenThread();
        initialize_memory();




    }


    public void FirstTimeScreen()
    {
        if(zz_Config.TEST)
            Log.d("_#FIRST_TIME_SCREEN", "FirstTimeScreen: aa_activity_StartUp -- A ");

        firstTimeThread.start();

        if(zz_Config.TEST)
            Log.d("_#FIRST_TIME_SCREEN", "FirstTimeScreen: aa_activity_StartUp -- B ");

    }
    public void SplashTimeScreen()
    {
        if(zz_Config.TEST)
            Log.d("_#SPLASH_SCREEN", "SplashTimeScreen: aa_activity_StartUp -- A ");
        splash_thread=true;
        splashScreenThread.start();

        if(zz_Config.TEST)
            Log.d("_#SPLASH_SCREEN", "SplashTimeScreen: aa_activity_StartUp -- B ");
    }


    public void Thread_Kiler(View view){


        int j =0;


        if(zz_Config.TEST)
            Log.d("_#_TT_TEST", " Inside T test"+start_thread);


        if(splashScreenThread!=null){

            if(splashScreenThread.isAlive())
            {
                aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER=true;
            }

            while (splashScreenThread.isAlive())
            {

                if(zz_Config.TEST)
                     Log.d("_#_TT_TEST", " Spalsh Thread Life"+splashScreenThread.isAlive()+"::"+aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER);
            }


        }



        if(firstTimeThread!=null){

            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER=true;

            while (firstTimeThread.isAlive())
            {

                if(zz_Config.TEST)
                   Log.d("_#_TT_TEST", " firstTimeThread Thread Life"+firstTimeThread.isAlive()+"::"+aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER);
            }


        }



/*
        while (firstTimeThread.isAlive())
        {

            if(zz_Config.TEST)
                Log.d("_#_TT_TEST", " first  Alive test"+firstTimeThread.isAlive());
        }

        while (splashScreenThread.isAlive())
        {

            if(zz_Config.TEST)
                Log.d("_#_TT_TEST", " splash  Alive test"+firstTimeThread.isAlive());
        }

*/
        if(zz_Config.TEST)
            Log.d("_#_TT_TEST", " Alive test"+firstTimeThread.isAlive());

        firstTimeThread=null;
        splashScreenThread=null;
        aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER=false;


    }


    public void free_memory(){
        if(zz_Config.TEST){

            Log.d("_#_APP_MEM_FREE", "from stop: MyApplication Mem Free A");

        }

        map.clear();
        map=null;
        map_folder.clear();
        map_folder=null;

        names.clear();
        names=null;

        names_folder.clear();
        names=null;


        if(zz_Config.TEST){

            Log.d("_#_APP_MEM_FREE", "from stop: MyApplication Mem Free B");

        }


      //  aa_activity_StartUp.intent=null;


    }

    void reset_variables_start_UP()
    {
        free_memory();
        aa_activity_StartUp.aaStartUp=null;
        Runtime.getRuntime().gc();
    }


    private static void FirstTimeThread_redirect()
    {






            if(zz_Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "FirstTimeThread_redirect()B1 : aa_activity_StartUp");

            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.intent = new Intent(aa_activity_StartUp.aaStartUp, aa_activity_home_screen.class);


            aa_activity_StartUp.aaStartUp.startActivity(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.intent);


            if(zz_Config.TEST)
                Log.d("_###_REDIR_FIRST_SCREEN", "FirstTimeThread_redirect()B2 : aa_activity_StartUp");








    }



    private static class FirstTimeThread extends Thread {
        @Override
        public void run()
        {

            if(zz_Config.TEST)
                Log.d("_##_FIRST_TIME", " Inside FirstTimeThread Thread :  aa_activity_StartUp A");



            int j=0;



            long   length =(long) (zz_Config.TIME_DELAY_START_SCREEN/zz_Config.TIME_DELAY_START_SCREEN_STEP);





            try
            {
                for(long i = 0; i<zz_Config.TIME_DELAY_START_SCREEN_STEP && !aa_activity_StartUp.aaStartUp.ab_fileManagerStart.THREAD_STOPPER; i++)
                {
                    Thread.sleep(length);
                    if (zz_Config.TEST)
                        Log.d("_##_FIRST_TIME", " CS :   aa_activity_StartUp  :: " +i);
                }
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }





///////////////////////////////////////////////////////////////////////////////////////////////
            try
            {
                aa_activity_StartUp.aaStartUp.ab_fileManagerStart.semaphore.acquire();
            }
            catch (InterruptedException e)
            {
                 e.printStackTrace();
            }


            if(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE!=2)
            {
                if (zz_Config.TEST)
                    Log.d("_##_FIRST_TIME", " CS :   aa_activity_StartUp  :: "+ aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE);

                aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE = 1;

                FirstTimeThread_redirect();
            }
            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.semaphore.release();
///////////////////////////////////////////////////////////////////////////////////////////////////



            if(zz_Config.TEST)
                Log.d("_##_FIRST_TIME", " Inside FirstTimeThread Thread :  aa_activity_StartUp B");


        }

    }

    private static void redirectToNewScreen_splash()
    {

        if(zz_Config.TEST)
        {
            Log.d("_#REDIR_SPLASH_SCREEN", "redirectToNewScreen_splash() A : aa_activity_StartUp"+aa_activity_StartUp.aaStartUp);

        }



        aa_activity_StartUp.aaStartUp.ab_fileManagerStart.intent = new Intent(aa_activity_StartUp.aaStartUp, aa_activity_home_screen.class);
        aa_activity_StartUp.aaStartUp.startActivity(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.intent);



        if(zz_Config.TEST)
            Log.d("_#REDIR_SPLASH_SCREEN", "redirectToNewScreen_splash() B : aa_activity_StartUp");


    }

    private static class SplashScreenThread extends Thread {
        @Override
        public void run()
        {


           if(zz_Config.TEST)
                Log.d("_###_SPLASH_THREAD_A", " Inside SplashScreenThread :   aa_activity_StartUp A");

            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.getVideo("aa_activity_StartUp");




            if(zz_Config.TEST)
            {


                Log.d("_###_SPLASH_THREAD", " Hash Inside SplashScreenThread :   aa_activity_StartUp C"+ Arrays.toString(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.map.entrySet().toArray()));
                Log.d("_###_SPLASH_THREAD", " Hash Inside SplashScreenThread :   aa_activity_StartUp C"+ Arrays.toString(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.names.toArray()));

            }


            try
            {
                aa_activity_StartUp.aaStartUp.ab_fileManagerStart.semaphore.acquire();
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

                if(aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE!=2)
                {
                    if (zz_Config.TEST)
                        Log.d("_###_SPLASH_THREAD", " CS :   aa_activity_StartUp  :: "+ aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE);

                    aa_activity_StartUp.aaStartUp.ab_fileManagerStart.TOKEN_SCENE_CHANGE = 1;

                    redirectToNewScreen_splash();
                }

            aa_activity_StartUp.aaStartUp.ab_fileManagerStart.semaphore.release();

            if(zz_Config.TEST)
                Log.d("_###SPLASH_THREAD", " Inside SplashScreenThread :   aa_activity_StartUp D");


        }
    }


///////////  Video File reading //////////////////////////////////////////////////////
    public void getfile(File dir) throws InterruptedException {



        File listFile[] = dir.listFiles();
        Boolean video_flag=false;
        Integer count=0;

        if (listFile != null && listFile.length > 0)
        {
            Thread.sleep(zz_Config.TIME_DELAY_INSIDE_THREAD_START);

            Thread.sleep(zz_Config.TIME_DELAY_INSIDE_THREAD_HOME);



            for (int i = 0; !THREAD_STOPPER &&(i < listFile.length) ; i++)
            {

                if (listFile[i].isDirectory())
                {
                    //fileList.add(listFile[i]);
                    try {
                        Thread.sleep(0);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    getfile(listFile[i]);

                }
                else
                {


                    if (
                            listFile[i].getName().endsWith(".3gp")
                                    || listFile[i].getName().endsWith(".avi")
                                    || listFile[i].getName().endsWith(".flv")
                                    || listFile[i].getName().endsWith(".m4v")
                                    || listFile[i].getName().endsWith(".mkv")
                                    || listFile[i].getName().endsWith(".mov")

                                    || listFile[i].getName().endsWith(".mp4")
                                    || listFile[i].getName().endsWith(".mpeg")
                                    || listFile[i].getName().endsWith(".mpg")
                                    || listFile[i].getName().endsWith(".mts")
                                    || listFile[i].getName().endsWith(".vob")



                                    || listFile[i].getName().endsWith(".webm")
                                    || listFile[i].getName().endsWith(".wmv")


                    )

                    // if (listFile[i].getName().endsWith(".webm"))

                    {


                        video_flag=true;
                        count++;



                        // names.add(listFile[i].toString());
                        //  mAttachmentList.add(new AttachmentModel(listFile[i].getName()));
                    }
                }
            }

            if(video_flag)
            {
                String string_path = dir.toString();
                String[] parts = string_path.split("/");

                List<String> values = new ArrayList<String>();
                values.add(dir.toString());
                values.add(count.toString());

                names.add(parts[parts.length - 1]);
                // names.add("WMV");
                map.put(parts[parts.length - 1], values);


            }







        }
        //return fileList;
    }
    private void getInbox()
    {
        // holder.text.setTextColor(Color.RED);



        List<String> values_1 = new ArrayList<String>();
        values_1.add("Inbox.path");
        values_1.add("12");
        names.add("Inbox");
        map.put("Inbox", values_1);

        List<String> values_2 = new ArrayList<String>();
        values_2.add("Inbox.path");
        values_2.add("12");
        names.add("Private Folder");

        map.put("Private Folder", values_2);

    }
    public void getVideo(String activity_name)
    {
        names.clear();
        map.clear();
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        if(zz_Config.TEST)
        {
            Log.d("_#__GET_VID_ROOT_DIR A", "Root Dir: "+dir.toString()+"_::_"+activity_name);
        }


        getInbox();


        try
        {
            getfile(dir );
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
        if(zz_Config.TEST)
        {
            Log.d("_#__GET_VID_ROOT_DIR B", "Root Dir: "+dir.toString()+"_::_"+activity_name);
        }


    }
///////////  Video File reading //////////////////////////////////////////////////////















}
