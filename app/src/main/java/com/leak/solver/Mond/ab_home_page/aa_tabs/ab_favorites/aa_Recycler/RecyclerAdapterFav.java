package com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_activity_home_screen;
import com.leak.solver.Mond.za_global.MyApplication;
import com.leak.solver.Mond.zz_Config;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterFav extends RecyclerView.Adapter<ViewHolderFav> {

    private List<DirectoryFav> directories;
    private Context context;



    public RecyclerAdapterFav(List<DirectoryFav> directories, Context context)
    {
        this.directories = new ArrayList<>();
        this.directories = directories;
        this.context = context;


    }

    public void setRecyclerAdapter(List<DirectoryFav> directories, Context context)
    {
        this.directories = directories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderFav onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ab_d_slice_frag_fav_recycler_item, viewGroup, false);


        return new ViewHolderFav(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderFav viewHolderFav, final int i)
    {


        DirectoryFav directoryFav = this.directories.get(i);
        viewHolderFav.video_count.setText(directoryFav.video_count);
        final aa_activity_home_screen aa_activity_home_screen = ((aa_activity_home_screen)context);
        viewHolderFav.dir_name.setText(directoryFav.dir_name+"_"+i);


   ////     List<String> values = new ArrayList<String>();
       // values = aa_activity_home_screen._global_StartUP.ab_fileManagerStart.map.get(aa_activity_home_screen._global_StartUP.ab_fileManagerStart.names.get(i));

        Integer i5 = new Integer(i);
        Integer i6 = new Integer(0);
        if(i==0)
        {


            viewHolderFav.dir_name.setTextColor(Color.parseColor("#008000"));
          //   Log.d("#__INDEX", aa_activity_home_screen._global_StartUP.ab_fileManagerStart.names.get(i)+" : "+i+":"+ values.get(0)+"  "+i5+":"+i6+";"+i5.equals(i6));

        }
        else
        {

            viewHolderFav.dir_name.setTextColor(Color.parseColor("#000000"));
        }


        viewHolderFav.slice.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {



                List<String> values = new ArrayList<String>();

                values = ((aa_activity_home_screen) context)._global_StartUP.ab_fileManagerStart.map.get( ((aa_activity_home_screen) context)._global_StartUP.ab_fileManagerStart.names.get(i));

               // Paramter_Parcelable paramter_parcelable=null;
             //   paramter_parcelable = new Paramter_Parcelable(values.get(0),values.get(1));
              //  ((MyApplication) (MyApplication.getContext())).map.clear();
           //     ((MyApplication) (MyApplication.getContext())).names.clear();

                if(zz_Config.TEST)
                {
                    Toast.makeText(context, "Item " + (i ) + " clicked  :: "+values.get(0),
                            Toast.LENGTH_SHORT).show();

                    Log.d("_##YES", "onClick: "+values.get(0));
                }

                ((aa_activity_home_screen) context).fileManager_home_fav.viewPager.setCurrentItem(2);

               // ((MyApplication) (MyApplication.getContext())).StartThread_files(values.get(0).toString());

               // ViewGroup slidingTabStrip =  (ViewGroup) (MyApplication.getContext()).tabLayout.getChildAt(0);

   /*
                for (int i = 0; i <  (MyApplication.getContext()).tabLayout.getTabCount(); i++) {
                    View tab = slidingTabStrip.getChildAt(i);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
                    if(i==1)
                        layoutParams.weight = 0;
                    else
                        layoutParams.weight = 1;
                    tab.setLayoutParams(layoutParams);


                }
    */

            }
        });






    }





    @Override
    public int getItemCount() {
        return directories.size();
    }


}
