package com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.leak.solver.Mond.R;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_activity_home_screen;
import com.leak.solver.Mond.ab_home_page.aa_tabs.aa_home_screen_PagerAdapter;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler.DirectoryFav;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapterFav;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.leak.solver.Mond.ab_home_page.aa_tabs.ac_folderview.aa_Recycler.RecyclerAdapter_folder;
import com.leak.solver.Mond.zz_Config;

import java.util.ArrayList;
import java.util.List;

public class FileManager_Home_fav {




    ///fav folder

    public ViewPager                                                                        viewPager=null;
    TabLayout                                                                        tabLayout=null;
    ImageButton                                                                          back =null;
    aa_home_screen_PagerAdapter                                    aa_home_screen_pagerAdapter=null;




    public RecyclerView recyclerViewFav;
    public RecyclerAdapterFav recyclerAdapterFav;
    public List<DirectoryFav> directories =new ArrayList<>();
    public SwipeRefreshLayout mSwipeRefreshLayoutFav;




    public Thread Fill_dir_or_read_dir=null;

    public Thread Full_Read_Dir =null;

    public Thread No_Read_Dir =null;


    Handler handler_full_read_handler=null;

    Handler handlerNo_Read_Dir=null;



    public void Init_Full_Read_Dir( Context context)
    {
        //  Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();



        handler_full_read_handler = new Handler(Looper.myLooper());



        Runnable runnable = () ->
        {



                fullread_exe();

        };
        Full_Read_Dir =  new Thread(runnable);

        Full_Read_Dir.start();

    }



    public void  Init_Half_Read_Dir( Context context)
    {
        //  Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();



        handlerNo_Read_Dir = new Handler(Looper.myLooper());



        Runnable runnable = () ->
        {



            halfread_exe();

        };
        No_Read_Dir =  new Thread(runnable);

        No_Read_Dir.start();

    }



    void fullread_exe()
    {

            if(zz_Config.TEST)
            {


                Log.d("_#FILL_DATA", "StartUpScreenThread_dir_fill : aa_activity_home_screen A");
            }

            if(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )
            aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.getVideo("aa_activity_home_screen");


            directories.clear();

            for (int i = 0;(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER)  && (i <aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.size()) ; i++)
            {

                List<String> values = new ArrayList<String>();
                values = aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.map.get(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.get(i));
                //  Log.d("DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names.get(i));
                DirectoryFav directoryFav = new DirectoryFav(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.get(i), values.get(1) + " Videos");
                directories.add(directoryFav);
            }



            if(zz_Config.TEST)
                Log.d("_#FILL_DATA", "StartUpScreenThread_dir_fill : aa_activity_home_screen B" );

        Runnable r = new Runnable() {
            @Override
            public void run()
            {

                /*
                int i=0;
                while(true){

                    if(zz_Config.TEST)
                        Log.d("_#FILL_DATA", "Handle " +i++);



                }*/
                   enable_tab(aa_activity_home_screen.mContext);
                  if(zz_Config.TEST)
                      Log.d("_#FILL_DATA", "Handler StartUpScreenThread_dir_fill : aa_activity_home_screen C" );

            }
        };

        if(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )

             handler_full_read_handler.post(r);

        if(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )
           handler_full_read_handler.removeCallbacksAndMessages(null);




    }


    void halfread_exe()
    {

        if(zz_Config.TEST)
        {


            Log.d("_#FILL_DATA", "StartUpScreenThread_dir_fill : aa_activity_home_screen A");
        }



        directories.clear();

        for (int i = 0;(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER)  && (i <aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.size()) ; i++)
        {

            List<String> values = new ArrayList<String>();
            values = aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.map.get(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.get(i));
            //  Log.d("DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names.get(i));
            DirectoryFav directoryFav = new DirectoryFav(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.get(i), values.get(1) + " Videos");
            directories.add(directoryFav);
        }



        if(zz_Config.TEST)
            Log.d("_#FILL_DATA", "StartUpScreenThread_dir_fill : aa_activity_home_screen B" );

        Runnable r = new Runnable() {
            @Override
            public void run()
            {

                /*
                int i=0;
                while(true){

                    if(zz_Config.TEST)
                        Log.d("_#FILL_DATA", "Handle " +i++);



                }*/
                enable_tab(aa_activity_home_screen.mContext);
                if(zz_Config.TEST)
                    Log.d("_#FILL_DATA", "Handler StartUpScreenThread_dir_fill : aa_activity_home_screen C" );

            }
        };

        if(!aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )

            handlerNo_Read_Dir.post(r);

        if(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.THREAD_STOPPER )
            handlerNo_Read_Dir.removeCallbacksAndMessages(null);




    }





    public FileManager_Home_fav() {
        this.directories = new ArrayList<>();
    }

    public void enable_tab(AppCompatActivity appCompatActivity)
    {

        aa_home_screen_pagerAdapter=null;
        viewPager=null;
        back=null;
        tabLayout=null;
        Runtime.getRuntime().gc();
        aa_home_screen_pagerAdapter=new aa_home_screen_PagerAdapter(appCompatActivity.getSupportFragmentManager(),
                appCompatActivity);

        viewPager = (ViewPager) appCompatActivity.findViewById(R.id.viewpager);
        back = (ImageButton) appCompatActivity.findViewById(R.id.backb);


        if(zz_Config.TEST)
            Log.d("_#ENABLE_TAB", "enable_tab  : aa_activity_home_screen A" );




        viewPager.setAdapter(aa_home_screen_pagerAdapter);
        // Give the TabLayout the ViewPager
        tabLayout = (TabLayout) appCompatActivity.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);




        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 2 : {
                        back.setVisibility(View.VISIBLE);
                        break;
                    }

                    default:{
                        back.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
/*
        ViewGroup slidingTabStrip =  (ViewGroup)  tabLayout.getChildAt(0);
        for (int i = 0; i < tabLayout.getTabCount(); i++)
        {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            if(i==2)
                layoutParams.weight = 0;
            else
                layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);


        }*/


        viewPager.setCurrentItem(1);

        if(zz_Config.TEST)
            Log.d("_#ENABLE_TAB", "enable_tab : aa_activity_home_screen B" );



    }








    public void StartUpScreenThread_dir_fill(Boolean full_read,Boolean update_dir_no_layout, Context context)
    {
        //  Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();



        Handler handler = new Handler(Looper.myLooper());



        Runnable runnable = () -> {

            if(zz_Config.TEST)
                Log.d("_#FILL_DATA", "StartUpScreenThread_dir_fill : aa_activity_home_screen A" );


            if(full_read && aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.home_thread) {

                aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.getVideo("aa_activity_home_screen");
            }

            directories.clear();

                for (int i = 0; (i <aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.size()) && aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.home_thread; i++)
                {

                    List<String> values = new ArrayList<String>();
                    values = aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.map.get(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.get(i));
                    //  Log.d("DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names.get(i));
                    DirectoryFav directoryFav = new DirectoryFav(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.names.get(i), values.get(1) + " Videos");
                    directories.add(directoryFav);
                }



            if(zz_Config.TEST)
                Log.d("_#FILL_DATA", "StartUpScreenThread_dir_fill : aa_activity_home_screen B" );


           /*
            if(zz_Config.TEST)
            {
                Log.d("_#DIR_CONTENT_HASH", "Hash: "+ ((MyApplication) getApplication()).map.toString());
                Log.d("_#DIR_CONTENT_NAME", "Hash: "+ ((MyApplication) getApplication()).names.toString());

            }
*/



            handler.post(new Runnable() {
                @Override
                public void run()
                {

                    if(update_dir_no_layout)
                    {
                       // recyclerAdapter_folder.setRecyclerAdapter(directories_folder,getContext());
                        // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
                       // recyclerAdapter_folder.notifyDataSetChanged();
                      //  mSwipeRefreshLayout_folder.setRefreshing(false);
                    }
                    else
                    {
                        if(aa_activity_home_screen.mContext._global_StartUP.ab_fileManagerStart.home_thread)
                        enable_tab(aa_activity_home_screen.mContext);
                    }



                    /*
                                               recyclerAdapter_folder.setRecyclerAdapter(directories_folder,getContext());
                            // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
                            recyclerAdapter_folder.notifyDataSetChanged();
                            mSwipeRefreshLayout_folder.setRefreshing(false);

                     */


                    if(zz_Config.TEST)
                        Log.d("_#FILL_DATA", "Handler StartUpScreenThread_dir_fill : aa_activity_home_screen C" );

                }
            });


            //

        };
        Fill_dir_or_read_dir =  new Thread(runnable);
        Fill_dir_or_read_dir.start();


    }






}
