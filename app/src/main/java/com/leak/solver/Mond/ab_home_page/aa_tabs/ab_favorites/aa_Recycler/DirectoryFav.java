package com.leak.solver.Mond.ab_home_page.aa_tabs.ab_favorites.aa_Recycler;

public class DirectoryFav {
    public DirectoryFav(String dir_name, String video_count) {
        this.dir_name = dir_name;
        this.video_count = video_count;
    }
    public String dir_name;


    public String getDir_name() {
        return dir_name;
    }

    public void setDir_name(String dir_name) {
        this.dir_name = dir_name;
    }

    public String getVideo_count() {
        return video_count;
    }

    public void setVideo_count(String video_count) {
        this.video_count = video_count;
    }

    public String video_count;
}
